const fs = require('fs');
const lt = require(`${__dirname}/../src/config/layouts.json`)
const child_process = require('child_process');
const fse = require('fs-extra');

function copyDir(srcDir, destDir, text="")
{ 
    // console.log(text, fs.existsSync( srcDir ) )
    if(fs.existsSync( srcDir ) )
    {
        fse.copySync( srcDir, destDir );
    }
}
function init_module(module_name, url)
{
    // add dirs if not exists
    if(!fs.existsSync(`${__dirname}/../src/modules`))
        fs.mkdirSync(`${__dirname}/../src/modules`)
    
    const module_dir = `${__dirname}/../src/modules/${module_name}`
    if(!fs.existsSync(module_dir))
    {    
        if(!fs.existsSync(`${__dirname}/../external`))
            fs.mkdirSync(`${__dirname}/../external`)        
        fs.mkdirSync(`${__dirname}/../external/${module_name}`)

        // copy source from git 
        child_process.execSync(`git clone ${url} -b main "${__dirname}/../external/${module_name}"`); 

        // instal from "external"    
        fs.mkdirSync(module_dir)
        fs.mkdirSync(`${module_dir}/assets`)
        fs.mkdirSync(`${module_dir}/public`)
        fs.mkdirSync(`${module_dir}/views`)
        fs.mkdirSync(`${module_dir}/widgets`)
    
        const srcviews    = `${__dirname}/../external/${module_name}/src/components/views`
        const destviews   = `${module_dir}/views`;
        copyDir(srcviews, destviews, "views")
    
        const srcwidgets  = `${__dirname}/../external/${module_name}/src/components/widgets`
        const destwidgets = `${module_dir}/widgets`;
        copyDir(srcwidgets, destwidgets, "widgets")
       
        const srcassets    = `${__dirname}/../external/${module_name}/src/components/assets`
        const destassets   = `${module_dir}/assets`;
        copyDir(srcassets, destassets, "assets")

        // copy module's layouts.json
        const srclayouts    = `${__dirname}/../external/${module_name}/src/components/layouts.json`
        const destlayouts   = `${module_dir}/layouts.json`;
        fs.copyFileSync( srclayouts, destlayouts );

        // edit layouts.json
        const data = require( destlayouts )
        lt.modules[module_name] = data
        lt.views            = {...lt.views, ...data.views}
        lt.widgets          = {...lt.widgets, ...data.widgets}
        lt.extentions       = {...lt.extentions, ...data.extentions}
        lt["widget-area"]   = {...lt["widget-area"], ...data["widget-area"]}

        // update schema        
        if(data.schema)
        {
            Object.keys(data.schema).forEach(name =>
            {
                if(lt.schema[name])
                {
                    lt.schema[name].apollo_fields = {
                        ...lt.schema[name].apollo_fields,
                        ...data.schema[name].apollo_fields
                    }
                }
               else
               {
                    lt.schema[name] = data.schema[name]
               }
            })
        }

        //routing
        if(data.routing)
        {
            Object.keys(data.routing).forEach(menu =>
            {
                data.routing[menu].map((route, i) =>
                {
                    if(!Array.isArray(lt.routing[menu]))
                        lt.routing[menu] = []
                    lt.routing[menu].unshift(route) 
                })
            }) 
        }
        fs.writeFileSync( 
            `${__dirname}/../src/config/layouts.json`, 
            JSON.stringify(lt, null, 2 ) 
        );
        
        //dicrionaries
        const srcLang = `${__dirname}/../external/${module_name}/src/components/lng/ru-RU.json`;
        const lng = `${__dirname}/../src/config/ru-RU.json`
        const ruOrig = require(srcLang)
        let ru = require(lng)    
        ru = {...ru, ...ruOrig}  
        fs.writeFileSync( 
            lng, 
            JSON.stringify(ru, null, 2 ) 
        );  

        //assets
        const srcpassets    = `${module_dir}/public/assets`;
        const destpassets   = `${__dirname}/../public/assets`
        copyDir(srcpassets, destpassets)

        //node-packages
        const packageFile = `${__dirname}/../external/${module_name}/src/components/package.json`;
        if (fs.existsSync(packageFile)) {
            const packageObject = require(packageFile);
            for (const name in packageObject.dependencies) {
                const version = packageObject.dependencies[name];
                child_process.execSync(`npm i --no-save ${name}@${version}`);
            }
        }

    }
    else
    {
        console.log(`Module ${module_name} already exists. For reinsall module run: npm run reinstall ${module_name}`)
    }        
}

const args =  process.argv;
if(args[2] && args[3] && `${args[3]}`.substring(0, 19) === "https://gitlab.com/")
{
    //const module_name = `${args[2]}`
    //const splits = `${args[3]}`.split("/")
    //console.log( splits[splits.length - 1] )  
    //console.log( lt.modules[module_name] )  
    init_module( `${args[2]}`, `${args[3]}`) 
}
else
{
    console.error("Укажите название плагина и его местонахождение на https//:gitlab.com, который надо установить")
}

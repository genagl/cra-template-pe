'use strict';
var path = require('path');
const webpack = require("webpack");
const CKEditorWebpackPlugin = require( '@ckeditor/ckeditor5-dev-webpack-plugin' );

module.exports = {
  entry: './src/index.js',
  output: {
    filename: 'bundle.js',
    path: path.resolve(__dirname, 'dist')
  },  
  module: {
    rules: [
      // any other rules
      {
        // Exposes jQuery for use outside Webpack build
        test: require.resolve('jquery'),
        use: [
          {
            loader: 'expose-loader',
            options: 'jQuery'
          },
          {
            loader: 'expose-loader',
            options: '$'
          }
        ],         
        loaders: [
          {
              test: /[\/\\]node_modules[\/\\]some-module[\/\\]index\.js$/,
              loader: "imports-loader?this=>window"
          }
        ]
      }
    ]
  }, 
  externals: 
  {
    jquery: "https://code.jquery.com/jquery-3.2.1.min.js"
  },
  plugins: [
    new CKEditorWebpackPlugin( {
        language: 'ru',
        addMainLanguageTranslationsToAllAssets: true
    } ),
    // other plugins,
    new webpack.ProvidePlugin( {
      $: 'jquery',
      jQuery: 'jquery',
      'window.jQuery': 'jquery'
    } ),
  ]   
};
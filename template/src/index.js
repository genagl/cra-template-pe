import React from "react"
import ReactDOM from "react-dom"
import App from "./App"
const config = require("./config/config.json")
const layoutConfig = require("./config/layouts.json")
const dictionary = require("./config/ru-RU.json")

ReactDOM.render( 
  <App 
    config={config}
    layoutConfig={layoutConfig}
    dictionary={dictionary}
  />,
  document.getElementById("root"),
)

#
<div class="pe-first">
	<div class="md-main__inner md-grid">
		<div class="hero">
			<img src="assets/img/first-page.png">
		</div>
		<div class="hero">
			<h1>
				Строим живые миры
			</h1>
			<p>
				Инструмент сборки масштабируемых пользовательских приложений и экосистем в философии микросервисной архитектуры и на основе технологий GraphQL и JWT
			</p>
			<div class="pe-flex-flex">
				<a href="/install/react-install/" class="md-button">
					Быстрый старт
				</a>
				<a href="/getting-start/" class="md-button md-button--primary">
					Документация
				</a>
			</div>
			<div class="pe-flex-flex small">
				<span>image from <a class="a" target="_blank" href="https://www.freepik.com/">freepik</a></span>
			</div>
		</div>
	</div>
</div>
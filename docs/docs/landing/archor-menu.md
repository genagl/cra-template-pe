Чтобы создать Якорное меню необходимо 

1. Создать новую Секцию
2. В модальном окне выбрать «Якорное меню» в разделе Layout

![anchor_menu_0](/assets/img/anchor_menu_0.jpg)

## Создание кнопок меню

Создать кнопки меню непосредственно в панели «Якорного меню» нельзя.

Каждая кнопка задаётся в разделе «Меню» каждой Секции, ссылка на которую должна быть в меню.

![anchor_menu_3](/assets/img/anchor_menu_3.jpg)
*(1) В панеле каждой Секции есть таб «Меню»*

1. Заходим в Пенель редактирования нужной Секции и жмём на кнопу «Меню»

![anchor_menu_4](/assets/img/anchor_menu_4.jpg)
*(2) Таб панели «Меню» каждой Секции*

1. Надпись на кнопке Якорного меню. Подключён к мульти-язычному переводчику. Т.е. если Вы напишите «Title» то на странице увидите кнопку «Заголовок» (см. раздел «Локализация»)
2. FontAwesome-иконка на кнопке справа от надписи
3. Уникальный идентификатор кнопки и Секции. 
4. Включать кнопку в Якорное меню или нет?
5. Программный ID Секции. Показывается для отладки.


## Поведение на различных устройствах отображения

Меню отображается в 2-х режимах:

1. Лента кнопок (если ширина экрана более 720 пикселей) 	
2. Кнопка «гамбургер» с выпадающим вертикальным списком кнопок (если ширина экрана менее 720 пикселей)

## Панель управления

![anchor_menu_1](/assets/img/anchor_menu_1.jpg) 
*(3)*


1. Горизонтальное выравнивание

2. Фиксация меню сверху. Если Секция якорного меню не первая, то в этом режиме меню фиксируется (прилипает к верху странице) в момент прокрутки Пользователем страницы до этой Секции и ниже. 

3. Эффект размытия контента страницы, которое попало под область меню (работает только в режиме зафиксированного меню сверху страницы)

4. Степень непрозрачности области меню, сквозь которую проступает размытое содержимое страницы под. Этот манипулятор скрыт, если манипулятор (3) отключён.

5. Уровень размытия содержимого страницы под. Этот манипулятор скрыт, если манипулятор (3) отключён.

6. Добавить картинку (лого) и тексовый заголовок слева от меню.

7. Загрузить изображение или указать URL-ссылку на него. Этот манипулятор скрыт, если манипулятор (6) отключён.

8. Текст заголовка. Этот манипулятор скрыт, если манипулятор (6) отключён.

9. Включив эту кнопку Вы зафиксируете шапку: независимо от состояния манипулятора (1) шапка всегда будет прижата влево. Этот манипулятор скрыт, если манипулятор (6) отключён.

10. Добавить css-классы к каждой кнопке меню. Например текст **bg-light text-danger** сделает все кнопки белыми c красными надписями насильно. Список доступных классов берёте из документации [Bootstrap-5](https://getbootstrap.com/docs/5.1/getting-started/introduction/) 

11. Стилизируем CSS каждой кнопки (подробности [здесь](/style-css/#css) )

12. Стилизация контейнера всей Секции. (см. «Стилизация кнонтейнера Секции» )

13. Стилизация контейнера всего меню 


## Как стилизовать разные элементы меню?

![anchor_menu_6](/assets/img/anchor_menu_6.jpg)
*(4) Области стилизации разными блоками*

При использовании инструментов css-стилизации необходимо понимать - на что повлияют Ваши манипуляции. Цифровые маркеры взяты от изображения (3)

11 - Стилизация каждой кнопки меню

12 - Стилизация контейнера контента Секции

13 - Стилизация самой Секции

!!! note
	Поэкспериментируйте. Задайте каждому инструменту по параметру и посмотрите результат:
	
	11 - **border: 1px solid red** (красная обводка вокруг каждой кнопки меню)
	
	12 - **border: 1px solid green** (зелёная обводка вокруг области меню)
	
	13 - **border: 1px solid blue** (синяя обводка вокруг секции, в которую включено меню)
	
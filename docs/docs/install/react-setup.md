# Настройка React-клиента

Разработчики ProtopiaEcosystem создали несколько рабочих модулей, которые устанавливаются прямо «из коробки» и расширяют Ваше PE-приложение дополнительной многопользовательской функциональностью.

Некоторые из этих модулей для полноценной работы требуют только React-клиента (например PE-Вёрстка - настраиваем статические посадочные страницы и публикуем приложение как статический сайт).

Однако большинство из модулей требует наличия серверных клиентов. В настоящее время каждый модуль требует собственного серверного клиента. Некоторые работают при поддержке NodeJS-серверов, другие - серверов на базе CMS Wordpress.

При выборе конфигурации приложения обращайте внимание на то, каким серверным клиентом поддерживаются выбранные Вами модули. Настройки соединения с сервером указываются в файле config.json.

### Конфигурационный файл **config.json**

!!! Warning "Внимание!"
	Если на этапе установки react-клиента Вы ответили на вопросы инсталлятора, то потребность в редактировании config.json отпадает.
	
	
	
- **Местоположение:** `/src/config/config.json`

- **Функционал:** 
	- Хранение секретных ключей, ключей API. 
	- URL удалённых клиентов.
	
- **Структура. Пример использования:**
??? Success  "Минимальная конфигурация config.json"
	```json
	{
		"server_url": "http://ux-api.protopia-home.ru/graphql",	
		// URI Вашего серверного клиента
		
		"assertion_token": "",									
		// JWS-token Вашего клиента для удалённого подключения к другим 
		// приложениям Вашей PE-экосистемы. В стандартном режиме 
		// однонодового использования PE оставьте поле пустой строкой
																
		"link_type": "http",
		// Тип http-протокола Вашего react-клиента. В следующих версиях 
		// поле устареет, а пока: Да здавствует лень программистов!
		
		"app_url": "http://localhost:3000",						
		// URI Вашего react-клиента. Поле необходимо для отправки автоматических
		// электронных писем пользователям. Например — для верификации новой
		// учётной записи в двухконтурной системе регистрации
		
		"yandex_map_api_key": "",								
		// Если хотите, чтоб работало обратное гео-позиционирование (определение 
		// адреса по координатам) - озаботьтесь:
		// https://yandex.ru/dev/maps/jsapi/doc/2.1/quick-start/index.html#get-api-key
		
		"gitlab_private_token": "f4FBDvKV-BzHMpFP75jk"
		// Это ключ доступа к закрытой веткt git, в 
		// которую сыпятся замечания Пользователей
		// о багах и предложения по улучшению продукта.
	}
	```


### Файл настройки **layouts.json**

!!! Warning "Внимание!"
	Установщик приложения и все подключаемые модули сами конфигурируют layout.json. Поэтому по-умолчанию редактирования файла настройки не требуется.

- **Местоположение:** `/src/config/layouts.json`

- **Функционал. Структура. Пример использования:**
??? cite "**app** — Главные параметры приложения."
	```json
	"app": {
		"title": "Protopia Ecosystems", // Заголовок приложения
		"name": "Protopia Ecosystems", 	// Имя на вкладке браузера
		"description": "",				// Описание приложения
		"external_systems": [],			// Список поддерживаемых серверных клиентов 
										// ("node", "wp", "telegramm", 
										// 		"facebook", "vk")
		"scopes": "",					//
		"adapter": "wp",				//
		"user_model": "User",			// не изменяемое значение
		"help_url": "https://genagl.gitlab.io/cra-template-pe/",// источник 
										// файлов помощи в react-клиенте
		"roles": [						// Список доступных пользовательских ролей
		  {
			"_id": "subscriber",		// Наблюдатель (незалогиненный пользователь)
			"title": "subscriber"	
		  },
		  {
			"_id": "administrator",		// Администратор
			"title": "administrator"
		  },
		  {
			"_id": "pupil",				// Ученик ( модуль PE-Наставник )
			"title": "Pupil"
		  },
		  {
			"_id": "tutor",				// Учитель ( PE-Наставник )
			"title": "Tutor"
		  },
		  {
			"_id": "courseleader",		// Ведущий курса ( PE-Наставник )
			"title": "CourseLeader"
		  },
		  {
			"_id": "editor",			// Редактор
			"title": "Editor"
		  }
		]
	}
	```
	
??? cite "template — Настройки графического шаблона."
	```json
	{
		"menu_left": 1,
		// наличие (1 или true) или отсутствие (0 или false) левого
		// бокового меню на всех страницах приложения. Если 0 — все
		// разрешения левого меню в настройках страниц в Карте сайта
		// игнорируются
		
		"header": 1,
		// наличие (1 или true) или отсутствие (0 или false) верхней
		// шапки приложения с логотипом, названием, верхним меню и 
		// кнопкой логирования/доступом в личный кабинет Пользователя
		
		"icon": "/assets/img/037-talkshow.svg",
		// Логотип приложения 
		
		"icon_width": 25,
		// ширина в пикселях области проецирования логотипа приложения
		
		"icon_height": 25,
		// высота в пикселях области проецирования логотипа приложения
		
		"avatar": "/assets/img/user1.svg",
		// умолчальный аватар залогированного Пользователя 
		
		"login": "/login",
		// страница логирования Пользователя
		
		"style": "/assets/css/style.css",
		// Каскадная таблица стилей приложения
		
		"styles": [ 
		// варианты кастомизации стилей, доступных по адресу /settings
		  {
			"_id": 1,
			"title": "Standart",
			"url": "/assets/css/style.css"
		  },
		  {
			"_id": 2,
			"title": "Light",
			"url": "/assets/css/style_light.css"
		  } 
		],
		"left_menu": "pictogramm"
		// тип отрисовки левого меню приложения. Варианты — "pictogramm", "hierarhical"
	  }
	```
??? cite "Настройки подключённых модулей."
	```json
	
	```
	
??? cite  "Список Экранов, Виджетов, Расширений и Гнёзд Виджетов."
	```json
	
	```
	
??? cite  "Модель данных."
	```json
	
	```
	
??? cite  "Карта сайта."
	```json
	
	```
	
!!! Warning "Особое внимание!"
	Изменения в структуре jaout.json вступают в силу только в **отключённом** режиме `«Особые настройки» — «Получать данные с сервера?»` [см. модуль «Администрирование»](/admin-module-views/#_5)
	
 
	
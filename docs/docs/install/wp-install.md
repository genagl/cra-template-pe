# Установка Wordpress-клиента

## Установка

1. Установим CMS Wordpress как в [классической инструкции](https://codex.wordpress.org/Установка_WordPress) или [популярной](https://wp-kama.ru/handbook/wordpress/ustanovka-wordpress)

2. На жёсткий диск своего компьютера скачиваем [отсюда](https://gitlab.com/wp37/pe-core) плагин PE-Core

3. По адресу `адрес сайта/wp-admin/plugins.php` жмём сначала «добавить новый», а потом «загрузить плагин»

4. В списке плагинов у PE Core жмём «Активировать»

5. Нам помогут ещё несколько сторонних плагинов:

	- [Add Local Avatar](https://wordpress.org/plugins/add-local-avatar/) - аватарки Пользователей
	
	- [Media Category Library](https://wordpress.org/plugins/media-category-library/) - Когда Пользователи начнут замусоривать Вашу Медиа-библиотеку, легко отделять зёрна от плевел. 
	
	- [SVG Support](https://wordpress.org/plugins/svg-support/) - популярный векторный формат иконок и логотипов SVG
	
	
## Настройка

- По адресу `адрес сайта/wp-admin/admin.php?page=pe_core_page` Вписываем те настройки, которые Вам будут нужны в оформлении приложения. Тоже самое можно делать на [странице Настроек](/admin-module-views/#_5) Вашего React-клиента.

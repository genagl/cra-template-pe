# ProtopiaEcosystem CRA template

Create-react-app template for initial ProtopiaEcosystem web console

Please refer to its documentation: [User Guide](http://ux.protopia-home.ru/)

--------

## Create new App:
1.
```
npx create-react-app my-app --template pe

cd my-app

npm run install
```
2. Answer the questions:

    - **put your server url:** 

        GraphQL uri of your PE-API. fro example: *http://ux-api.protopia-home.ru/graphql*

    - **put your app's JWT-token:** 



    - **put "http" or "https" protocol of your client:** 

    - **put your client url:** 

    You can skip the answers to these questions. In this case, fill in the fields of the **src/config/config.json**

3. **npm start** to start local or **npm run build** to production build

--------

## There are 2 base modules in the current configuration:

- https://gitlab.com/genagl/react-pe-basic-module
- https://gitlab.com/genagl/react-pe-admin-module

--------

## Other modules from the team of the Protopia Home Design Bureau:

[Jitsi module](https://gitlab.com/genagl/react-pe-jitsi-module)

[Online education module](https://gitlab.com/genagl/pe-oraculi-module)

[Landing layout module](https://gitlab.com/genagl/react-pe-landing-module)

